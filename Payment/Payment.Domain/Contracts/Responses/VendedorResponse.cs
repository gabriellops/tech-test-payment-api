﻿namespace Payment.Domain.Contracts.Responses
{
    public class VendedorResponse : BaseResponse
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}