﻿namespace Payment.Domain.Interfaces
{
    public interface IBaseCRU<Request, Response>
    {
        Task<Response> PostAsync(Request request);
        Task<Response> GetById(int id);
        Task<Response> PutAsync(Request request, int? id);
    }
}