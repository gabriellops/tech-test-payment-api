﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Infrastructure.Contexts
{
    public class PaymentContext : DbContext
    {
        public PaymentContext(DbContextOptions<PaymentContext> options) : base(options) 
        {
        }

        public DbSet<VendaEntity> VendaEntities { get; set; }
        public DbSet<VendedorEntity> VendedorEntities { get; set; }
        public DbSet<PedidoEntity> PedidoEntities { get; set;}
    }
}