﻿namespace Payment.Domain.Enums
{
    public enum EStatusVenda
    {
        Aguardando_pagamento,
        Pagamento_aprovado,
        Cancelada,
        Enviado_para_transportadora,
        Entregue
    }
}