﻿namespace Payment.Domain.Entities
{
    public class VendedorEntity : BaseEntity
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public virtual ICollection<VendaEntity> VendaEntities { get; set; }
    }
}