# Swagger
## Funcionamento da API
[Swagger](https://ibb.co/tHbKV3q), [Método POST](https://ibb.co/ykn5s9m), [Método GET](https://ibb.co/NNyPJhv), [Método PUT](https://ibb.co/nM9rdd3)

## Instalação
```bash
# Clone este repositório
$ git clone <https://gitlab.com/gabriellops/tech-test-payment-api>

# Acesse a pasta por meio do visualStudio
$ Abra a solution Payment.sln

# Execute o projeto
$ Abra a solution Payment.sln

# O servidor inciará na porta: 7025 - acesse https://localhost:7025/swagger/index.html
```
