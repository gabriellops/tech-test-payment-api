﻿using Payment.Domain.Contracts.Requests;
using Payment.Domain.Contracts.Responses;

namespace Payment.Domain.Interfaces.Services
{
    public interface IVendaService : IBaseCRU<VendaRequest, VendaResponse>
    {
    }
}