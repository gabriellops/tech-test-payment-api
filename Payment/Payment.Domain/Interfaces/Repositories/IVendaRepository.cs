﻿using Payment.Domain.Entities;

namespace Payment.Domain.Interfaces.Repositories
{
    public interface IVendaRepository : IBaseCRU<VendaEntity, VendaEntity>
    {
    }
}
