﻿using AutoMapper;
using Payment.Domain.Contracts.Requests;
using Payment.Domain.Contracts.Responses;
using Payment.Domain.Entities;
using Payment.Domain.Enums;
using Payment.Domain.Interfaces.Repositories;
using Payment.Domain.Interfaces.Services;

namespace Payment.Domain.Services
{
    public class VendaService : IVendaService
    {
        public readonly IMapper _mapper;
        private readonly IVendaRepository _vendaRepository;

        public VendaService(IMapper mapper, IVendaRepository vendaRepository)
        {
            _mapper = mapper;
            _vendaRepository = vendaRepository;
        }

        public async Task<VendaResponse> PostAsync(VendaRequest request)
        {
            var requestVenda = _mapper.Map<VendaEntity>(request);
            var registarVenda = await _vendaRepository.PostAsync(requestVenda);

            if (requestVenda is null)
                throw new Exception("Venda não existe");

            return _mapper.Map<VendaResponse>(registarVenda);
        }

        public async Task<VendaResponse> GetById(int id)
        {
            var vendaBancoDeDados = await _vendaRepository.GetById(id);

            if (vendaBancoDeDados is null)
                throw new Exception("Venda não existe");

            return _mapper.Map<VendaResponse>(vendaBancoDeDados);
        }

        public async Task<VendaResponse> PutAsync(VendaRequest request, int? id)
        {
            var vendaBancoDeDados = await _vendaRepository.GetById((int)id);

            if (vendaBancoDeDados is null)
                throw new Exception("Venda não existe");

            switch (vendaBancoDeDados.Status)
            {
                case EStatusVenda.Aguardando_pagamento:
                    if (request.Status == EStatusVenda.Pagamento_aprovado || request.Status == EStatusVenda.Cancelada)
                        vendaBancoDeDados.Status = request.Status;
                    else
                        throw new Exception("Alteração de status inválida, válida somente para Pagamento Aprovado ou Cancelada");
                    break;

                case EStatusVenda.Pagamento_aprovado:
                    if (request.Status == EStatusVenda.Enviado_para_transportadora || request.Status == EStatusVenda.Cancelada)
                        vendaBancoDeDados.Status = request.Status;
                    else
                        throw new Exception("Alteração de status inválida, válida somente para Enviado para Transportadora ou Cancelada");
                    break;

                case EStatusVenda.Enviado_para_transportadora:
                    if (request.Status == EStatusVenda.Entregue)
                        vendaBancoDeDados.Status= request.Status;
                    else
                        throw new Exception("Alteração de status inválida, válida somente para Entregue");
                    break;

                default:
                    throw new Exception("Atualização de status inválida");
            }

            var vendaRegistrada = await _vendaRepository.PutAsync(vendaBancoDeDados, null);

            return _mapper.Map<VendaResponse>(vendaRegistrada);
        }
    }
}