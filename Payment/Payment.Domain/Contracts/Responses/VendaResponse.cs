﻿using Payment.Domain.Entities;
using Payment.Domain.Enums;

namespace Payment.Domain.Contracts.Responses
{
    public class VendaResponse
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public List<PedidoEntity> Pedidos { get; set; }
        public EStatusVenda Status { get; set; }

        public int VendedorId { get; set; }
        public string VendedorNome { get; set; }
    }
}