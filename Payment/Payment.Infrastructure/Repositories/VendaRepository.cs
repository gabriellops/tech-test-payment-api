﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;
using Payment.Domain.Interfaces.Repositories;
using Payment.Infrastructure.Contexts;

namespace Payment.Infrastructure.Repositories
{
    public class VendaRepository : IVendaRepository
    {
        private readonly PaymentContext _context;

        public VendaRepository(PaymentContext context)
        {
            _context = context;
        }

        public async Task<VendaEntity> PostAsync(VendaEntity request)
        {
            await _context.VendaEntities.AddAsync(request);
            await _context.SaveChangesAsync();

            return request;
        }

        public async Task<VendaEntity> GetById(int id)
        {
            return await _context.VendaEntities.Where(prop => prop.Id == id).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<VendaEntity> PutAsync(VendaEntity request, int? id = null)
        {
            _context.VendaEntities.Update(request);
            await _context.SaveChangesAsync();

            return request;
        }
    }
}