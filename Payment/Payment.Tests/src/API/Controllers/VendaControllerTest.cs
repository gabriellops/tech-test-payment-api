using AutoFixture;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Payment.Controllers;
using Payment.Domain.Contracts.Requests;
using Payment.Domain.Contracts.Responses;
using Payment.Domain.Interfaces.Services;
using Payment.Tests.Configs;

namespace Payment.Tests.src.API.Controllers
{
    [Trait("Controller", "Controller de Vendas")]
    public class VendaControllerTest
    {
        private readonly Mock<IVendaService> _mockVendaService = new Mock<IVendaService>();
        private readonly Fixture _fixture;

        public VendaControllerTest ()
        {
            _mockVendaService = new Mock<IVendaService>();
            _fixture = FixtureConfig.Get();
        }
    
        [Fact(DisplayName = "Registra uma nova venda")]
        public async Task Post()
        {
            var request = _fixture.Create<VendaRequest>();
            var contractResponse = _fixture.Create<Task<VendaResponse>>();

            _mockVendaService.Setup(mock => mock.PostAsync(request)).Returns(contractResponse);

            var controller = new VendaController(_mockVendaService.Object);
            var response = await controller.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(response);

            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Busca um venda por id")]
        public async Task GetById()
        {
            var contractResponse = _fixture.Create<VendaResponse>();

            _mockVendaService.Setup(mock => mock.GetById(It.IsAny<int>())).ReturnsAsync(contractResponse);

            var controller = new VendaController(_mockVendaService.Object);
            var response = await controller.GetByIdAsync(contractResponse.Id);

            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var vendaResponse = Assert.IsType<VendaResponse>(objectResult.Value);

            Assert.Equal(vendaResponse.Id, contractResponse.Id);
        }


        [Fact(DisplayName = "Edita uma venda exisente")]
        public async Task Put()
        {
            var request = _fixture.Create<VendaRequest>();
            var contractResponse = _fixture.Create<Task<VendaResponse>>();

            _mockVendaService.Setup(mock => mock.PutAsync(request, contractResponse.Result.Id)).Returns(contractResponse);

            var controller = new VendaController( _mockVendaService.Object);
            var resultado = await controller.PutAsync(contractResponse.Result.Id, request);

            var objectResult = Assert.IsType<OkObjectResult>(resultado.Result);

            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
    }
}