﻿using AutoMapper;
using Payment.Domain.Contracts.Requests;
using Payment.Domain.Contracts.Responses;
using Payment.Domain.Entities;

namespace Payment.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile() 
        {
            CreateMap<VendaRequest, VendaEntity>();

            CreateMap<VendaEntity, VendaResponse>();
        }
    }
}