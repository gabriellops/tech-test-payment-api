using Microsoft.AspNetCore.Mvc;
using Payment.Domain.Contracts.Requests;
using Payment.Domain.Contracts.Responses;
using Payment.Domain.Interfaces.Services;

namespace Payment.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService _vendaService;

        public VendaController(IVendaService vendaService)
        {
            _vendaService = vendaService;
        }

        /// <summary>
        /// Registrar uma venda
        /// </summary>
        /// <remarks></remarks>
        /// <param name="request">Dados da venda</param>
        /// <returns>Objeto rec�m-criado</returns>
        /// <response code="201">Sucess</response>

        [HttpPost("registrar-venda")]
        [ProducesResponseType(201)]
        public async Task<ActionResult> PostAsync([FromBody] VendaRequest request)
        {
            var entity = await _vendaService.PostAsync(request);
    
            return Created(nameof(PostAsync), new { id = entity.Id,  });
        }

        /// <summary>
        /// Buscar uma venda
        /// </summary>
        /// <remarks></remarks>
        /// <param name="id">Id da venda</param>
        /// <returns>Objeto encontrado</returns>
        /// <response code="200">Sucess</response>
        /// 
        [HttpGet("buscar-venda/{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<VendaResponse>> GetByIdAsync([FromRoute] int id)
        {
            var resultado = await _vendaService.GetById(id);

            return Ok(resultado);
        }

        /// <summary>
        /// Atualizar uma venda existente
        /// </summary>
        /// <remarks></remarks>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns>Objeto encontrado</returns>
        /// <response code="200">Sucess</response>
        [HttpPut("atualizar-venda/{id}")]
        [ProducesResponseType(204)]
        public async Task<ActionResult<VendaResponse>> PutAsync([FromRoute] int id, [FromBody] VendaRequest request)
        {
            var resultado = await _vendaService.PutAsync(request, id);

            return Ok(resultado);
        }
    }
}