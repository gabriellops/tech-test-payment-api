namespace Payment.Tests.src.Infrastructure.Repositories
{
    [Trait("Repositório", "Repositório de Vendas")]
    public class VendaRepositoryTest
    {
        [Fact(DisplayName = "Registra uma nova venda")]
        public async Task Post_RetornaTrue()
        {

        }

        [Fact(DisplayName = "Busca um venda por id")]
        public async Task GetById_RetornaTrue()
        {

        }


        [Fact(DisplayName = "Edita uma venda exisente")]
        public async Task Put_RetornaTrue()
        {

        }
    }
}