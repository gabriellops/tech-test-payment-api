﻿using Newtonsoft.Json;
using Payment.Domain.Entities;
using Payment.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Contracts.Requests
{
    public class VendaRequest
    {
        public DateTime Data { get; set; }

        [Required(ErrorMessage = "'Pedidos' deve possuir pelo menos 1 item")]
        public List<PedidoEntity> Pedidos { get; set; }
        public EStatusVenda Status { get; set; }

        public int VendedorId { get; set; }
        public string VendedorNome { get; set; }
    }
}