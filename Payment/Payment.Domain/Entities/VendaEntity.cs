﻿using Newtonsoft.Json;
using Payment.Domain.Enums;

namespace Payment.Domain.Entities
{
    public class VendaEntity : BaseEntity
    {

        public DateTime Data { get; set; }
        public List<PedidoEntity> Pedidos { get; set; }
        public EStatusVenda Status { get; set; } = EStatusVenda.Aguardando_pagamento;

        public int VendedorId { get; set; }
        public string VendedorNome { get; set; }
    }
}