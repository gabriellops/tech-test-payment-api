/*using AutoFixture;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Payment.Domain.Contracts.Requests;
using Payment.Domain.Contracts.Responses;
using Payment.Domain.Entities;
using Payment.Domain.Interfaces.Repositories;
using Payment.Domain.Services;
using Payment.Tests.Configs;
using System.Linq.Expressions;
using System;

namespace Payment.Tests.src.Domain.Services
{
    public class VendaServiceTest
    {

        N�O DEU TEMPO :'(

        private readonly Mock<IVendaRepository> _mockVendaRepository = new Mock<IVendaRepository>();
        private readonly IMapper _mapper;
        private readonly Fixture _fixture;

        public VendaServiceTest()
        {
            _mockVendaRepository = new Mock<IVendaRepository>();
            _mapper = MapConfig.Get();
            _fixture = FixtureConfig.Get();
        }

        [Fact(DisplayName = "Registra uma nova venda")]
        public async Task Post()
        {
            var request = _fixture.Create<VendaEntity>();
            var contractResponse = _fixture.Create<Task<VendaResponse>>();

            _mockVendaRepository.Setup(mock => mock.PostAsync(request)).Returns(contractResponse);

            var repository = new VendaService(_mapper, _mockVendaRepository.Object);
            var response = await repository.PostAsync(request);

            var objectResult = Assert.IsType<CreatedResult>(response);

            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact(DisplayName = "Busca um venda por id")]
        public async Task GetById()
        {
            var contractResponse = _fixture.Create<VendaResponse>();

            _mockVendaRepository.Setup(mock => mock.GetById(It.IsAny<int>())).ReturnsAsync(contractResponse);

            var repository = new VendaService(_mapper, _mockVendaRepository.Object);
            var response = await repository.GetByIdAsync(contractResponse.Id);

            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var vendaResponse = Assert.IsType<VendaResponse>(objectResult.Value);

            Assert.Equal(vendaResponse.Id, contractResponse.Id);
        }


        [Fact(DisplayName = "Edita uma venda exisente")]
        public async Task Put()
        {
            var entity = _fixture.Create<Task<VendaEntity>>();
            var contractRequest = _fixture.Create<VendaRequest>();

            _mockVendaRepository.Setup(mock => mock.PutAsync(entity, entity.Result.Id)).Returns(contractRequest);

            var repository = new VendaService(_mapper, _mockVendaRepository.Object);
            var resultado = await repository.PutAsync(entity, contractRequest.Result.Id);

            var objectResult = Assert.IsType<OkObjectResult>(resultado.Result);

            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
    }
}*/