﻿using System.ComponentModel.DataAnnotations;

namespace Payment.Domain.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}