﻿using Payment.Domain.Entities;

namespace Payment.Domain.Interfaces.Repositories
{
    public class IVendedorRepository : IBaseCRU<VendedorEntity, VendedorEntity>
    {
        public Task<VendedorEntity> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Task<VendedorEntity> PostAsync(VendedorEntity request)
        {
            throw new NotImplementedException();
        }

        public Task<VendedorEntity> PutAsync(VendedorEntity request, int? id)
        {
            throw new NotImplementedException();
        }
    }
}