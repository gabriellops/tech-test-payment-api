﻿namespace Payment.Domain.Entities
{
    public class PedidoEntity : BaseEntity
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
    }
}